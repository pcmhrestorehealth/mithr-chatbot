STATES = {}


def move_to(state, new_position):
    return dict(state, position=new_position)


def extend_state_with(state, new_items):
    return dict(state, **new_items)


DEFAULT_USER_STATE = {
    "position": "pre-languages-intro"
}


def get_state_of(user_id, fallback_state=DEFAULT_USER_STATE):
    if user_id in STATES:
        return STATES[user_id]
    return dict(fallback_state, user_id=user_id)


def set_state_of(user_id, new_state):
    STATES[user_id] = new_state


def get_language_from_user(user_id):
    return get_language_from_state(get_state_of(user_id))


def get_language_from_state(state):
    return state.get("language", "en")


def get_user_id_from(state):
    return state['user_id']
