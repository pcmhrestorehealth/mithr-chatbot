import csv


def read_translations_file():
    result = {}
    with open('translations.csv', 'r') as translations_file:
        reader = csv.DictReader(translations_file)
        for row in reader:
            code = row['code']
            for column in row:
                if len(column) == 2:
                    language = column
                    try:
                        result[language][code] = row[language]
                    except KeyError:
                        result[language] = {code: row[language]}
    return result


TRANSLATIONS = read_translations_file()


def get_translations(language):
    return TRANSLATIONS.get(language)


TRIGGERS = {
    "0": "0️⃣",
    "1": "1️⃣",
    "2": "2️⃣",
    "3": "3️⃣",
    "4": "4️⃣",
    "5": "5️⃣",
    "6": "6️⃣",
    "7": "7️⃣",
    "8": "8️⃣",
    "9": "9️⃣"
}


def fancy_trigger(trigger):
    return TRIGGERS.get(trigger, "**" + trigger + "**")


def fancify(number):
    output = ""
    for char in number:
        output += fancy_trigger(char)
    return output
