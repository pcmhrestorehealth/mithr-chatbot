import logging
import os

import httpx

from config import TURN_TOKEN


def get_turn_token():
    if 'TURN_TOKEN' in os.environ:
        return os.environ.get('TURN_TOKEN')
    if TURN_TOKEN != "":
        return TURN_TOKEN
    return None


TOKEN = get_turn_token()


def is_turn_on():
    if 'TURN_OFF' in os.environ:
        return False
    return TURN_TOKEN is not None


MESSAGES_URL = 'https://whatsapp.turn.io/v1/messages'
HEADERS = {
    "Authorization": f"Bearer {TOKEN}",
}


def individual_message(to):
    return {
        "preview_url": True,
        "recipient_type": "individual",
        "to": to,
        "type": "text",
    }


def send_text_message(to, body):
    data = individual_message(to)
    data['text'] = {"body": body}

    httpx.post(MESSAGES_URL, headers=HEADERS, json=data)


def upload_binary_data(binary_data, content_type):
    url = 'https://whatsapp.turn.io/v1/media'
    headers = {
        "Authorization": f"Bearer {TOKEN}",
        "Content-Type": content_type
    }
    response = httpx.post(url, headers=headers, content=binary_data)
    logging.debug("Uploaded binary data. Received the following response:")
    logging.debug(response.json())
    return response.json().get('media', [{id: None}])[0]['id']


def send_image_message(to, media_id, caption=None):
    data = individual_message(to)
    data['image'] = {"id": media_id}
    if caption is not None:
        data['image']['caption'] = caption
    httpx.post(MESSAGES_URL, headers=HEADERS, json=data)
