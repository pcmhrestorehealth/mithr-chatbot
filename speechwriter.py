from statemachine import get_language_from_state
from translations import get_translations


def message(state, message_id, params=None):
    translations = get_translations_from_state(state)
    return retrieve_translation(message_id, translations)


def retrieve_translation(message_id, translations):
    if message_id in translations:
        return translations[message_id]
    else:
        return message_id


def messages(state, message_ids, params=None):
    translations = get_translations_from_state(state)
    translated = []
    for message_id in message_ids:
        translated.append(retrieve_translation(message_id, translations))
    return translated


def get_translations_from_state(state):
    language = get_language_from_state(state)
    translations = get_translations(language)
    return translations


SILENT_OUTPUT = []
TRIGGER_NEXT = ""
