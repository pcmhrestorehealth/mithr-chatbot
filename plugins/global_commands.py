from speechwriter import SILENT_OUTPUT, TRIGGER_NEXT
from statemachine import move_to


def flux(state, incoming):
    if incoming == "0":
        return (
            move_to(state, "initial"),
            SILENT_OUTPUT,
            TRIGGER_NEXT
        )
    raise NotImplementedError
