import csv
import logging

from speechwriter import message, SILENT_OUTPUT, TRIGGER_NEXT
from statemachine import extend_state_with, move_to
from translations import fancify


def read_providers():
    result = []
    with open('providers.csv', 'r') as providers_file:
        reader = csv.DictReader(providers_file)
        for row in reader:
            provider_id = row['No']
            specialties = row['Specialties'].split(';')
            place = row['Place']
            result.append({
                'provider_id': provider_id,
                'specialties': specialties,
                'place': place,
                'details': row
            })
    return result


PROVIDERS = read_providers()


def fits_filter(available, required):
    if required in available:
        return True
    if required == available:
        return True
    return False


def fits(filters, provider):
    if filters is None:
        return True
    for filter_key in filters:
        if not fits_filter(provider[filter_key], filters[filter_key]):
            return False
    return True


def filter_providers(filters, providers=PROVIDERS):
    return [provider for provider in providers if fits(filters, provider)]


def get_provider_by_id(chosen_provider_id):
    return [provider for provider in PROVIDERS if provider['provider_id'] == chosen_provider_id][0]


def get_specialties(filters=None, providers=PROVIDERS):
    specialties = set()
    for provider in filter_providers(filters, providers):
        specialties = specialties.union(provider['specialties'])
    return specialties


def get_places(filters=None, providers=PROVIDERS):
    places = set()
    for provider in filter_providers(filters, providers):
        places.add(provider['place'])
    return places


def get_specialty_options(place=None):
    filters = None
    if place is not None:
        filters = {'place': place}
    specialties = get_specialties(filters)
    options = {}
    for index, specialty in enumerate(specialties):
        options[str(index + 1)] = specialty
    return options


def get_location_options(specialty=None):
    filters = None
    if specialty is not None:
        filters = {'specialties': specialty}
    places = get_places(filters)
    options = {}
    for index, place in enumerate(places):
        options[str(index + 1)] = place
    return options


def convert_to_options_message(state, options):
    output = ""
    for option_index in options:
        output += fancify(option_index) + ": " + message(state, options[option_index]) + "\n"
    return output


def convert_providers_to_options(options):
    output = ""
    for option_index in options:
        output += fancify(option_index) + ": " + get_provider_by_id(options[option_index])["details"]["Name"] + "\n"
    return output


def return_to_main_menu(state):
    return (
        move_to(state, "initial"),
        SILENT_OUTPUT,
        TRIGGER_NEXT
    )


def prompt_specialty(state):
    options = get_specialty_options()
    return (
        extend_state_with(state, {"position": "choose-specialty", "options": options}),
        f'{message(state, "provider-search-intro")}\n'
        f'{convert_to_options_message(state, options)}\n\n'
        f'{message(state, "switch-language")}\n'
        f'{message(state, "go-back-to-main-menu")}'
    )


def prompt_location(specialty, state):
    options = get_location_options(specialty)
    return (
        extend_state_with(state,
                          {"position": "choose-location", "provider-specialty": specialty, "options": options}),
        f'{message(state, "provider-search-choose-location")}\n'
        f'{convert_to_options_message(state, options)}\n\n'
        f'{message(state, "go-back")}\n'
        f'{message(state, "go-back-to-main-menu")}'
    )


def prompt_choose_provider(chosen_location, chosen_specialty, state):
    providers_available = filter_providers({"place": chosen_location, "specialties": chosen_specialty})
    options = {str(i + 1): provider['provider_id'] for i, provider in
               enumerate(providers_available)}
    return (
        extend_state_with(state,
                          {
                              "position": "choose-provider",
                              "provider-specialty": chosen_specialty,
                              "provider-location": chosen_location,
                              "options": options
                          }),
        f'*{message(state, chosen_location)}*\n'
        f'{message(state, "provider-result-intro")}\n'
        f'{convert_providers_to_options(options)}\n'
        f'{message(state, "go-back")}\n'
        f'{message(state, "go-back-to-main-menu")}'
    )


def show_provider_details(chosen_provider_id, state):
    chosen_provider = get_provider_by_id(chosen_provider_id)
    logging.debug(chosen_provider)
    return (
        state,
        f'*{chosen_provider["details"]["Name"]}, {chosen_provider["details"]["Designation"]}*\n\n'
        f'{message(state, "address")}\n'
        f'{chosen_provider["details"]["Clinic"]}\n\n'
        f'{message(state, "appointment")}\n'
        f'{chosen_provider["details"]["Appointment"]}\n\n'
        f'{message(state, "schedule")}\n'
        f'{chosen_provider["details"]["Schedule"]}\n\n'
        f'{message(state, "fee")}\n'
        f'{chosen_provider["details"]["Fee"]}\n\n'
        f'{message(state, "go-back-to-main-menu")}'
    )


def flux(state, incoming):
    if state["position"] == "pre-provider-search":
        return prompt_specialty(state)
    if state["position"] == "choose-specialty":
        if incoming == "*":
            return return_to_main_menu(state)
        if incoming.lower() == "kn":
            return prompt_specialty(extend_state_with(state, {"language": "kn"}))
        if incoming.lower() == "en":
            return prompt_specialty(extend_state_with(state, {"language": "en"}))
        if incoming in state['options']:
            specialty = state['options'][incoming]
            return prompt_location(specialty, state)
    if state["position"] == "choose-location":
        if incoming == "*":
            return prompt_specialty(state)
        if incoming in state['options']:
            chosen_specialty = state['provider-specialty']
            chosen_location = state['options'][incoming]
            return prompt_choose_provider(chosen_location, chosen_specialty, state)
    if state["position"] == "choose-provider":
        if incoming == "*":
            return prompt_location(state['provider-specialty'], state)
        if incoming in state['options']:
            chosen_provider_id = state['options'][incoming]
            return show_provider_details(chosen_provider_id, state)

    raise NotImplementedError


if __name__ == "__main__":
    print(PROVIDERS)
    print(get_specialties(None))
    print(len(filter_providers({"specialties": "Family Physician"})))
