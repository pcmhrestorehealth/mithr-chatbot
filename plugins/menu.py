import logging

from speechwriter import message, SILENT_OUTPUT, TRIGGER_NEXT
from statemachine import move_to, extend_state_with

LANGUAGE_CHOICES = {
    "1": "en",
    "2": "kn"
}


def flux(state, incoming):
    if state['position'] == "pre-languages-intro":
        return (
            move_to(state, "languages-intro"),
            message(state, "languages-intro")
        )
    if state['position'] == "languages-intro":
        if incoming in LANGUAGE_CHOICES:
            return (
                extend_state_with(state, {"position": "initial", "language": LANGUAGE_CHOICES[incoming]}),
                SILENT_OUTPUT,
                TRIGGER_NEXT
            )
    if state['position'] == "initial":
        return (
            move_to(state, "general-welcome"),
            message(state, "general-welcome")
        )
    if state['position'] == "general-welcome":
        if incoming == "1":
            return (
                move_to(state, "pre-provider-search"),
                SILENT_OUTPUT,
                TRIGGER_NEXT
            )
        if incoming == "2":
            return (
                state,
                message(state, "covid-info")
            )
        if incoming == "3":
            return (
                state,
                message(state, "social-protection")
            )
        if incoming == "4":
            return (
                state,
                message(state, "promote-wellness")
            )
        if incoming == "5":
            return (
                move_to(state, "pre-feedback"),
                SILENT_OUTPUT,
                TRIGGER_NEXT
            )
        if incoming == "6":
            return (
                state,
                [
                    message(state, "share-following"),
                    message(state, "share-with-friends")
                ]
            )
        if incoming == "9":
            return (
                move_to(state, "change-language"),
                message(state, "change-language")
            )
    if state['position'] == "change-language":
        logging.debug("Change language triggered")
        if incoming in LANGUAGE_CHOICES:
            return (
                extend_state_with(
                    state,
                    {
                        "position": "initial",
                        "language": LANGUAGE_CHOICES[incoming]
                    }
                ),
                SILENT_OUTPUT,
                TRIGGER_NEXT
            )
    raise NotImplementedError
