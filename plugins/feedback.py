import logging

from config import FEEDBACK_RECIPIENTS
from gmail import send as send_email
from speechwriter import message, TRIGGER_NEXT
from statemachine import move_to, get_user_id_from


def handle_feedback(state, incoming):
    logging.debug("Received feedback" + incoming)
    send_email(FEEDBACK_RECIPIENTS, "Feedback about Mithr chatbot from " + get_user_id_from(state), incoming)


def flux(state, incoming):
    if state["position"] == "pre-feedback":
        return (
            move_to(state, "feedback"),
            message(state, "feedback-prompt")
        )

    if state["position"] == "feedback":
        return (
            move_to(state, "feedback-further"),
            message(state, "feedback-further")
        )
    if state["position"] == "feedback-further":
        if incoming != "0":
            handle_feedback(state, incoming)
        return (
            move_to(state, "initial"),
            message(state, "feedback-thanks"),
            TRIGGER_NEXT
        )

    raise NotImplementedError
