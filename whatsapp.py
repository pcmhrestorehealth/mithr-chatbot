from enum import Enum
from typing import List, Optional

from pydantic import BaseModel


class MessageType(str, Enum):
    audio = 'audio'
    contacts = 'contacts'
    document = 'document'
    image = 'image'
    location = 'location'
    text = 'text',
    unknown = 'unknown'
    video = 'video'
    voice = 'voice'


class Profile(BaseModel):
    name: Optional[str]


class Contact(BaseModel):
    profile: Profile
    wa_id: str


class Context(BaseModel):
    from_: str
    id: str
    mentions: Optional[List[str]]

    class Config:
        fields = {
            'from_': 'from'
        }


class Identity(BaseModel):
    acknowledged: str
    created_timestamp: int
    hash: str


class Text(BaseModel):
    body: str


class Location(BaseModel):
    latitude: float
    longitude: float
    address: str
    name: str
    url: str


class Address(BaseModel):
    street: Optional[str]
    city: Optional[str]
    state: Optional[str]
    zip: Optional[str]
    country: Optional[str]
    country_code: Optional[str]
    type: Optional[str]


class Email(BaseModel):
    email: Optional[str]
    type: Optional[str]


class IM(BaseModel):
    service: str
    user_id: str


class Name(BaseModel):
    first_name: Optional[str]
    middle_name: Optional[str]
    last_name: Optional[str]
    formatted_name: Optional[str]
    name_prefix: Optional[str]
    name_suffix: Optional[str]


class Org(BaseModel):
    company: Optional[str]
    department: Optional[str]
    title: Optional[str]


class Phone(BaseModel):
    phone: Optional[str]
    wa_id: Optional[str]
    type: Optional[str]


class Url(BaseModel):
    url: Optional[str]
    type: Optional[str]


class ContactCard(BaseModel):
    addresses: Optional[List[Address]]
    birthday: Optional[str]
    contact_image: Optional[str]
    emails: Optional[List[Email]]
    ims: Optional[List[IM]]
    name: Optional[Name]
    org: Optional[Org]
    phones: Optional[List[Phone]]
    urls: Optional[List[Url]]


class Metadata(BaseModel):
    pass


class Media(BaseModel):
    caption: Optional[str]
    file: Optional[str]
    filename: Optional[str]
    id: str
    metadata: Metadata
    mime_type: str
    sha256: str


class Audio(Media):
    pass


class Image(Media):
    pass


class Document(Media):
    pass


class Video(Media):
    pass


class Voice(Media):
    pass


class Sticker(Media):
    pass


class System(BaseModel):
    body: str


class Message(BaseModel):
    context: Optional[Context]
    from_: str
    id: str
    identity: Optional[Identity]
    timestamp: str

    type: MessageType

    audio: Optional[Audio]
    contacts: Optional[List[ContactCard]]
    document: Optional[Document]
    image: Optional[Image]
    location: Optional[Location]
    system: Optional[System]
    text: Optional[Text]
    video: Optional[Video]
    voice: Optional[Voice]

    system: Optional[System]

    class Config:
        fields = {
            'from_': 'from'
        }


class InboundWebhook(BaseModel):
    contacts: List[Contact]
    messages: List[Message]


class StatusNotification(BaseModel):
    pass
