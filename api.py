from fastapi import FastAPI

from bot import handle_incoming
from whatsapp import InboundWebhook

api = FastAPI()


@api.on_event("startup")
async def startup_event():
    pass


@api.get('/')
async def root():
    return {"message": "Hello, world"}


# InboundWebhook looks like this:
# contacts=[Contact(profile=Profile(name='Jenga'), wa_id='917890123456')]
# messages=[Message(context=None, from_='917890123456', id='ABEGkXgpYEcAAhAnPFIGzDjWKbYsTPo7g5ck',
# identity=None, timestamp='1610939421', type=<MessageType.text: 'text'>,
# audio=None, contacts=None, document=None, image=None, location=None, system=None,
# text=Text(body='Hi'), video=None, voice=None)]
@api.post('/turn/whatsapp')
async def root_receive(message: InboundWebhook):
    print(message)
    first_text = message.messages[0]
    handle_incoming(first_text.text.body, first_text.from_)
    return "done"


@api.get('/simple/{user}/{message}')
async def add_message(user: str, message: str):
    return handle_incoming(message, user)
