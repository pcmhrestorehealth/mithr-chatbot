import logging

from mediamanager import is_uploaded_media, get_turn_id
from plugins.feedback import flux as feedback_flux
from plugins.global_commands import flux as global_commands_flux
from plugins.menu import flux as menu_flux
from plugins.provider_search import flux as provider_search_flux
from speechwriter import message
from statemachine import get_state_of, set_state_of
from turn import send_text_message, is_turn_on, send_image_message

logging.basicConfig(level=logging.DEBUG)


def flux(state, incoming):
    try:
        return menu_flux(state, incoming)
    except NotImplementedError:
        pass
    try:
        return provider_search_flux(state, incoming)
    except NotImplementedError:
        pass
    try:
        return feedback_flux(state, incoming)
    except NotImplementedError:
        pass
    try:
        return global_commands_flux(state, incoming)
    except NotImplementedError:
        pass
    return state, message(state, "unknown-input")


def send(user_id, outbox):
    for outbox_item in outbox:
        if is_turn_on():
            if is_uploaded_media(outbox_item):
                send_image_message(user_id, get_turn_id(outbox_item))
            else:
                send_text_message(user_id, outbox_item)
        else:
            if is_uploaded_media(outbox_item):
                print("Would have send image at ", outbox_item, "to user", user_id)
            else:
                print("Would have send text ", outbox_item, "to user ", user_id)
    return outbox


def cast_to_list(incoming):
    if isinstance(incoming, list):
        return incoming
    return [incoming]


def handle_incoming(incoming, user_id):
    logging.debug("Incoming message from " + user_id + ": " + incoming)
    incoming = cast_to_list(incoming)
    state = get_state_of(user_id)
    logging.debug("current state:")
    logging.debug(state)
    outbox = []
    while incoming:
        state, outgoing, *incoming = flux(state, incoming[0])
        outbox.extend(cast_to_list(outgoing))
        logging.debug("One iteration complete. The outbox now contains " + str(len(outbox)) + " messages. State is:")
        logging.debug(state)
    set_state_of(user_id, state)
    return send(user_id, outbox)
