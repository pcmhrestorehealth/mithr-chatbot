import argparse
import json
import os

import magic

from turn import upload_binary_data, is_turn_on


def read_metadata():
    with open('assets.json', 'r') as metadata:
        return json.load(metadata)


def write_metadata(metadata):
    with open('assets.json', 'w') as metadata_file:
        json.dump(metadata, metadata_file)


def ensure_metadata_file_exists():
    if not os.path.exists('assets.json'):
        write_metadata({})


ensure_metadata_file_exists()
METADATA = read_metadata()


def reload_metadata():
    global METADATA
    METADATA = read_metadata()


def get_path_to_asset(filename):
    return os.path.join("assets", filename)


def upload_media_to_turn(filename):
    path = get_path_to_asset(filename)
    content_type = magic.from_file(path, mime=True)
    if is_turn_on():
        print("Uploading to turn")
        with open(path, 'rb') as media:
            content = media.read()
            media_id = upload_binary_data(content, content_type)
            return media_id
    else:
        print("Would have uploaded the following to turn")
        print("Content-type:", content_type)
        print("Path:", path)
        return "NOT_UPLOADED"


def associate_turn_id(filename, media_id):
    METADATA[filename] = dict(METADATA.get(filename, {}), id=media_id)
    write_metadata(METADATA)


def get_turn_id(filename):
    if filename in METADATA and METADATA[filename]['id'] != "NOT_UPLOADED":
        return METADATA[filename]['id']
    return None


def is_uploaded_media(filename):
    return get_turn_id(filename) is not None


def add_media(filename):
    media_id = upload_media_to_turn(filename)
    associate_turn_id(filename, media_id)


def do_upload(arguments):
    return add_media(arguments.path)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Manage media")
    subparsers = parser.add_subparsers()
    upload_parser = subparsers.add_parser('upload')
    upload_parser.add_argument('path')
    upload_parser.set_defaults(func=do_upload)

    args = parser.parse_args()
    args.func(args)
