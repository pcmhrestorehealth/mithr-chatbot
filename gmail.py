import yagmail

from config import EMAIL_FROM, EMAIL_PASS


def send(to, subject, contents):
    return yagmail.SMTP(EMAIL_FROM, EMAIL_PASS).send(to, subject, contents)
